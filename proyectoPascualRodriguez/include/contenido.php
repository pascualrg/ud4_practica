<?php

function contenido(){

print '<div class="contenido">';
print' <form action="include/procesa.php" method="post">';
print'      <label for="nomApell">Nombre y Apellidos</label>';
print'      <input type="text" name="nomApell" required><br><br>';
print'      <label for="sexo">Sexo: </label><br><br>';
print'      <input type="radio" name="sexo" value="Mujer" checked> Mujer <br>';
print'      <input type="radio" name="sexo" value="Hombre"> Hombre <br>';
print'      <input type="radio" name="sexo" value="Otro"> Otro <br><br>';
print'      ¿Cuántas peliculas has visto este mes? <input type="number" name="numPelis" min="0" max="20"><br><br>' ;
print'      ¿Cuántas estrellas le pondrías a nuestra web? (0 a 5) <input type="number" name="estrellas" min="0" max="5"><br><br>' ;
print'      <label for="mejorPeli">Para ti, ¿Cuál es la mejor película del siglo XXI entre estas? </label><br><br>';
print'      <select name="mejorPeli">';
print'      <option name="Ninguna">-- Ninguna --</option>';
print'      <option name="_zodiac">Zodiac</option>';
print'      <option name="_mysticRiver">Mystic Ryver</option>';
print'      <option name="_pianista">El pianista</option>';
print'      <option name="_dunkerque">Dunkerque</option>';
print'      <option name="_fauno">El laberinto del fauno</option>';
print'      <option name="_birdman">Birdman</option>';
print'      </select><br><br>';
print       '<label for="text">Digame la frase que mas le gusta del cine.</label><br><br>';
print       '<textarea name="texto" id="texto" placeholder="Aquí la frase." maxlength="200" cols="50" rows="10"></textarea><br><br>';
print       '<label for="generos">¿Qué generos son tus favoritos entre estos seis?</label><br><br>';
print       '<input type="checkbox" name="genero[]" value="Terror">Terror<br><br>';
print       '<input type="checkbox" name="genero[]" value="Drama">Drama<br><br>';
print       '<input type="checkbox" name="genero[]" value="Comedia">Comedia<br><br>';
print       '<input type="checkbox" name="genero[]" value="Suspense">Suspense<br><br>';
print       '<input type="checkbox" name="genero[]" value="Accion">Accion<br><br>';
print       '<input type="checkbox" name="genero[]" value="Documentales">Documentales <br><br>';
print       '<label for="dark">¿Vas a querer un cambio en la página al procesar la información?</label><br><br>';
print       '<input type="radio" name="modoOscuro" value="si" checked> Cambiar a version dark al procesar la información. <br><br>';
print       '<input type="radio" name="modoOscuro" value="no"> No cambiar nada al procesar la información. <br><br>';
print'      <input type="submit" value="Enviar formulario">';
print' </form>';
print '</div>';        

}

?>
