<?php

//las rutas relativas me dieron muchos problemas en este proyecto al tener el index.php fuera de la carpeta include. Así que empleé rutas absolutas sobre la carpeta htdocs. /PHP/Actividades/proyectoPascualRodriguez/..

function procesado(){


//CAMBIO DE ESTILO CON LOS RADIO BUTTONS.

if ($_REQUEST["modoOscuro"] == "si") {


    print '<style>';
    print 'header {border: 2px solid orange;background-color: slategrey;}';
    print 'body {background-color: dimgrey;}';
    print '.contenido {border: 2px solid orange;background-color: slategrey;}';
    print '.contenido img {margin: 10px;}';
    print '.logo {background: url("/PHP/Actividades/proyectoPascualRodriguez/img/logo_dark.svg");background-repeat: no-repeat;background-size: cover;width: 150px;height: 150px;display: block;text-indent: -9999px;cursor: default;}';
    print '.footer {background-color: slategray;border: 2px solid orange;}';
    print '.bloque {border: 2px solid orange;background-color: slategrey;}';
    print '</style>';

}
//

//INFORMACION PERSONAL
print '<div class="contenido">';
print '<h2>Información personal</h2>';
print"<p><b>Nombre y Apellidos:</b> $_REQUEST[nomApell]</p>";    
print"<p><b>Sexo:</b> $_REQUEST[sexo]</p>";
print '</div>';
//

//NUMERO DE PELICULAS
print '<div class="contenido">';

    if ($_REQUEST["numPelis"] == "") {

        print "<p>No has contestado al número de peliculas vistas este mes.</p>";
    }

    else {

        if ($_REQUEST["numPelis"] == 1) {

            print "<p>Has visto 1 pelicula.</p>";
            print"<img width='100px' src='/PHP/Actividades/proyectoPascualRodriguez/img/pelicula.svg'>";
        }
        
        else {
        
            print "<p>Has visto $_REQUEST[numPelis] peliculas.</p>";
            for ($i=0; $i<$_REQUEST["numPelis"]; $i++) {
        
                print"<img width='100px' src='/PHP/Actividades/proyectoPascualRodriguez/img/pelicula.svg'>";
        
            }
        
        }
    }

print '</div>'; 

//
//ESTRELLAS
print '<div class="contenido">';

    if ($_REQUEST["estrellas"] == "") {

        print"<p>No has valorado la web con una puntuación.</p>";

    }
        else {

            print "<p>Puntuación: $_REQUEST[estrellas]/5 estrellas.</p>";

            if ($_REQUEST["estrellas"] == 0) {

                print"<img width='25px' src='/PHP/Actividades/proyectoPascualRodriguez/img/sad.svg'>";
            }

            for ($i=0; $i<$_REQUEST["estrellas"]; $i++) {

                print"<img width='25px' src='/PHP/Actividades/proyectoPascualRodriguez/img/star.svg'>";

            }
    }

print '</div>'; 
//


//MEJORES PELICULAS
print '<div class="contenido">';

print "<h2>Tu pelicula favorita: $_REQUEST[mejorPeli]</h2>";
print"<img width='500px' src='/PHP/Actividades/proyectoPascualRodriguez/img/$_REQUEST[mejorPeli].png'>";


print '</div>'; 
//


//TEXTO (TEXTAREA)
print '<div class="contenido">';

$palabras = str_word_count("$_REQUEST[texto]", 1,'[áéíóúñ]' );


//La variable $palabras es una array con las palabras escritas por el usuario. Con la función count() se puede saber cuantas palabras se han generado en la aray: [0], [1], etc.
$numPalabras = count($palabras);

$array_cadena = str_word_count("$_REQUEST[texto]", 1, '[áéíóúñ]');


for ($i=0; $i<$numPalabras; $i++){


        if ($i%2==0){

            print "<span class='palabras1'>";
            print($array_cadena)[$i] . " ";
            print "</span>";
            
            }  
        
            else {
        
            print "<span class='palabras2'>";
            print($array_cadena)[$i] . " ";
            print "</span>";
            }
    }

print "<p>Nos has escrito <b>$numPalabras</b> palabras.";

print '</div>'; 
//

//GENEROS. He utilizado la información de esta página: https://makitweb.com/get-checked-checkboxes-value-with-php/ 
//         para documentarme hacerca de cómo utilizar el foreach y así utilizarlo para esta parte del ejercicio.
print '<div class="contenido">';
print"<p>Imagenes relacionadas con los generos que ha indicado que le gustan:<p/>";

    if (isset($_REQUEST["genero"])) {

        foreach($_REQUEST["genero"] as $genero) {

            print"<div class='genero'><img width='50px' src='/PHP/Actividades/proyectoPascualRodriguez/img/$genero.svg' alt='$genero'>";
            print"<span class='generoTexto'>$genero</span>";
            print"</div>";
            

        }
        
    }

    else {

        print"<p>No ha indicado ningun genero. Así que no hay ninguyna imágen.<p/><a href='../index.php'>Vuelva atrás para escoger algún género</a>";
    }

print '</div>'; 
//




//FEHCA Y HORA https://www.baulphp.com/convertir-fecha-php-en-espanol/ & https://www.aprenderaprogramar.com/index.php?option=com_content&view=article&id=855:funciones-php-de-fecha-hora-tiempo-time-date-formato-de-fecha-mktime-gmmkitme-ejemplo-cu00830b&catid=70&Itemid=193
// 0=Domningo, 1=Lunes, etc y "w" te devuelve un numero entre 0 y 6. 
//La "d" devuelve un numero entre 01 y 31.
//La "n" devuelve un numero entre 1 y 12. Así que para que escoja correctamente el mes dentro de la array, necesitaremos restar
//1 para que "empiece" desde 0. (Enero=0, Febrero=1).
//La "Y" devuelve el año con 4 digitos.
print '<div class="contenido">';

$diasSemana = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
 
print $diasSemana[date('w')].", ".date('d')." de ".$meses[date('n')-1]. " del ".date('Y') ;

print '</div>'; 
//

}

?>
